<link rel="stylesheet" href="{{ URL::asset('packages/barryvdh/elfinder/jquery/jquery-ui-1.12.0.css') }}" type="text/css">
<!-- elfinder css -->
<link rel="stylesheet" href="{{ URL::asset('packages/barryvdh/elfinder/css/elfinder.min.css')}}"    type="text/css">
<!-- jQuery and jQuery UI (REQUIRED) -->
<script src="{{ URL::asset('js/jquery.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ URL::asset('packages/barryvdh/elfinder/jquery/jquery-ui-1.12.0.js') }}" type="text/javascript" charset="utf-8"></script>
<!-- elfinder core -->
<script src="{{ URL::asset('packages/barryvdh/elfinder/js/elfinder.min.js') }}"></script>
<!-- elfinder default lang -->
<script src="{{ URL::asset('packages/barryvdh/elfinder/js/i18n/elfinder.en.js') }}"></script>


